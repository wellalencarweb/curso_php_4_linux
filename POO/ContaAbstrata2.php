<?php
 

 abstract class Conta 
 {
 	protected  $titular;
 	protected  $saldo;

 	public function __construct($titular,$saldo)
 	{

 		$this->titular = $titular;
 		$this->saldo   = $saldo;
		echo "Bem Vindo Sr.(a) $titular<br>";

 	}

 	final public function verSaldo()
 	{
		return $this->saldo;
	}

	
	abstract public function depositar($saldo);

	

	public function sacar($saldo)
	{
		
		if ($this->saldo >= $saldo) {

			echo "Saque Realizado com Sucesso!<br>";
			echo "Saldo anterior = {$this->saldo}<br>";
			$this->saldo -= $saldo;
			echo "Saldo atual    = {$this->saldo}<br><br>";

		} else {

			echo 'Saldo insuficiente <br>';
		}
	}

 }


 final class ContaCorrente extends Conta
 {

 	public function depositar($saldo)
	{
		
		echo "Deposito Realizado com Sucesso!<br>";
		echo "Saldo anterior = {$this->saldo}<br>";
		$this->saldo += $saldo;
		echo "Saldo atual    = {$this->saldo}<br><br>";
	}

 } 

 
 final class ContaPoupanca extends Conta
 {

 	const JUROS = 1.15;

 	public function depositar($saldo)
	{
		
		
		echo "Deposito Realizado com Sucesso!<br>";
		echo "Saldo anterior = {$this->saldo}<br>";
		$this->saldo += $saldo + ($saldo * self::JUROS);
		echo "Saldo atual com juros   = {$this->saldo}<br><br>";
	}

 }


$WellC = new ContaCorrente("Well",10);
$WellC->depositar(20);
$WellC->sacar(15);
echo "<hr>";

$WellP = new ContaPoupanca("Well",10);
$WellP->depositar(20);
$WellP->sacar(15);
