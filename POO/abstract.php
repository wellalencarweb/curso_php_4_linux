<?php 
	abstract class FormaGeometrica
	{
		public function getTipo() 
		{
			return $this->tipo;
		}

		public function getTamanho()
		{
			return $this->tamanho;
		}

		abstract public function calcularArea();
	}

	class Quadrado extends FormaGeometrica
	{
		public $tipo    = "Quadrado";
		public $tamanho;
		public $lado    = "20"; 

		public function __construct($tamanho)
		{
			$this->tamanho = $tamanho;
			$this->calcularArea();
			
			
		}

		public function calcularArea()
		{
			$this->tamanho = $this->lado * $this->lado;
		}

	}

	class Circulo extends FormaGeometrica 
	{
		public $tipo    = "Circulo";
		public $tamanho;	
		public $raio;

		public function __construct($raio)
		{
			$this->raio = $raio;
			$this->calcularArea();
		}

		public function calcularArea()
		{
			$this->tamanho = 3.14 * ($this->raio * $this->raio);
		}			
	}

	$quadrado = new Quadrado(15);
	$circulo  = new Circulo(7);

	echo $quadrado->getTipo();
	echo "<br>";

	$quadrado->calcularArea();	
	echo $quadrado->getTamanho();
	echo "<hr>";

	echo $circulo->getTipo();
	echo "<br>";

	$circulo->calcularArea();	
	echo $circulo->getTamanho();

	