<?php

interface Televisao
{
	public function aumentarVolume();
	public function diminuirVolume();
	public function trocarCanal($val);
	public function ligarDesligar();

}

class TV implements  Televisao
{
	public $ligada          = 0;  
	public $volume          = 0;
	public $canal           = 1;
	public $nCanais         = 60;
	public $maxVolume       = 10;

	
	public function aumentarVolume()
	{
		if($this->ligada == 1){
			
			if($this->volume < $this->maxVolume) {

				$this->volume += 1;
				echo "Volume Aumentado para: {$this->volume} <br>";
			
			} else {

				echo "Volume Maximo Atingido!<br>";
				
			}

		} else {

			echo "A TV nao esta ligada (nao da para aumentar o volume)!<br>";
		}
		
	}
	
	public function diminuirVolume()
	{

		if($this->ligada == 1){
			
			if($this->volume > 0) {

				$this->volume -= 1;
				echo "Volume Diminuido para: {$this->volume} <br>";
			
			} else {

				echo "Volume Minimo Atingido!<br>";
				
			}

		} else {

			echo "A TV nao esta ligada (nao da para aumentar o diminuir)!<br>";
		}

	}
	
	public function trocarCanal($val)
	{
		if($this->ligada == 1){

			if (($val < $this->nCanais) && ($val >= 1)) {

				$this->canal = $val;

				echo "Canal trocado para {$this->canal} <br>";

			} else {
				
				echo "Canal nao encontrado <br>";
			}

		} else {

			echo "A TV náo esta ligada. Nao e possivel trocar canal!! <br>";
		}

		$this->canal = $val;
	}
	
	public function ligarDesligar()
	{
		if($this->ligada == 0) {
			
			 $this->ligada = 1;
			 echo "TV Ligada!<br>";
		
		} else {

			$this->ligada = 0;
			echo "TV Desligada!<br>";
		}

	}



}

$tv = new TV();
$tv->ligarDesligar();
$tv->aumentarVolume();
$tv->aumentarVolume();
$tv->diminuirVolume();
$tv->trocarCanal(12);
$tv->trocarCanal(65);
echo "<hr>";
$tv->ligarDesligar();
$tv->aumentarVolume();
$tv->diminuirVolume();
$tv->trocarCanal(12);
