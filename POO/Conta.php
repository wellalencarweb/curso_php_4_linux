<?php

/**
* 
*/

class Conta implements funcoesConta
{
	
	public $nroConta;
	private $saldo = 0;

	public function verSaldo(){
		return $this->saldo;
	}

	public function depositar($saldo){
		$this->saldo += $saldo;
	}

	public function sacar($saldo){
		
		if ($this->saldo >= $saldo)
			$this->saldo -= $saldo;
		else
			echo 'Saldo insuficiente';
	}
	
}

$conta1 = new Conta();
$conta1->depositar(15);
$conta1->depositar(15);
$conta1->depositar(35);

echo $conta1->verSaldo();

$conta1->sacar(35);

echo '<br>';
echo $conta1->verSaldo();
echo '<br>';

$conta1->sacar(35);

echo '<br>';
echo $conta1->verSaldo();
echo '<br>';