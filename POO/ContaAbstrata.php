<?php
 

 abstract class Conta 
 {
 	protected  $titular;
 	protected  $saldo;

 	public function __construct($titular,$saldo)
 	{

 		$this->titular = $titular;
 		$this->saldo   = $saldo;
		echo "Bem Vindo Sr.(a) $titular<br>";

 	}

 	public function verSaldo()
 	{
		return $this->saldo;
	}

	public function depositar($saldo)
	{
		
		
		echo "Deposito Realizado com Sucesso!<br>";
		echo "Saldo anterior = {$this->saldo}<br>";
		$this->saldo += $saldo;
		echo "Saldo atual    = {$this->saldo}<br><br>";
	}

	public function sacar($saldo)
	{
		
		if ($this->saldo >= $saldo) {

			echo "Saque Realizado com Sucesso!<br>";
			echo "Saldo anterior = {$this->saldo}<br>";
			$this->saldo -= $saldo;
			echo "Saldo atual    = {$this->saldo}<br><br>";

		} else {

			echo 'Saldo insuficiente <br>';
		}
	}

 }


 final class ContaCorrente extends Conta
 {


 } 

 
 final class ContaPoupanca extends Conta
 {

 }


$Well = new ContaCorrente("Well",10);
$Well->depositar(20);
$Well->sacar(15);
echo "<hr>";
